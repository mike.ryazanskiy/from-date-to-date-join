with CTE1 AS
(
  SELECT 
    RANK() OVER (PARTITION BY [post_id] ORDER BY [views] ,[likes], [shares]) as [rank]
    ,*
  FROM [dbo].[posts_stats]
),
CTE2 AS
(
  select
    [dttm]
    ,[post_id]
    ,[views]
    ,[likes]
    ,[shares]
    ,[rank]
    ,[rank] - lag([rank], 1, 0) OVER (PARTITION BY post_id ORDER BY dttm) rr
  from CTE1
)
SELECT
  [post_id]
  ,[views]
  ,[likes]
  ,[shares]
  ,[dttm] effective_from
  ,LEAD([dttm], 1, '9999-12-31 23:59:59') OVER (PARTITION BY post_id ORDER BY dttm) effective_to
FROM CTE2
where rr <> 0
order by post_id, dttm

