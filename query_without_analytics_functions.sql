with cte as
(
SELECT 
 a.post_id, a.views, a.likes, a.shares,  a.dttm as dt_from_t,
 (
  select MIN(dttm) as dttm 
  from public.posts_stats b
  where a.post_id = b.post_id  and b.dttm > a.dttm
   and (a.views != b.views or a.likes != b.likes or a.shares != b.shares) 
 ) as dt_to
from public.posts_stats a
)
select post_id, "views", likes, shares,  min(dt_from_t) as dt_from,  coalesce(dt_to, '9999-12-31 23:59:59') as dt_to
from cte
group by post_id, views, likes, shares, coalesce(dt_to, '9999-12-31 23:59:59')  
order by post_id, dt_from
